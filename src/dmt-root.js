import React, {Component} from 'react';
import {ImageBackground, StyleSheet, View, Text} from 'react-native';
import {Icon} from 'native-base';
import { createDrawerNavigator, createAppContainer } from 'react-navigation';

import Home from './dmt-home';
import Partner from './dmt-stack-partner';
import Withdrawal from './dmt-stack-withdrawal';
import Guest from './dmt-stack-guest';

const headerDrawer=()=>(
  <View style={styles.container}>
    <View style={styles.headerContainer}>
      <ImageBackground source={require('../assets/img/logo.png')} style={{flex: 1, width: 280, justifyContent: 'center'}} >
        <Text style={styles.headerText}>Header Portion</Text>
        <Text style={styles.headerText}>You can display here logo or profile image</Text>
      </ImageBackground>
    </View>
  </View>
)

const RootDrawer = createDrawerNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      drawerIcon: <Icon type="FontAwesome" name="home" style={{fontSize: 20}}/>
    }
  },
  Guest: {
    screen: Guest,
    navigationOptions: {
      drawerIcon: <Icon type="FontAwesome" name="user" style={{fontSize: 20}}/>
    }
  },
  Partner: {
    screen: Partner,
    navigationOptions: {
      drawerIcon: <Icon type="FontAwesome" name="users" style={{fontSize: 20}}/>
    }
  },
  Withdrawal: {
    screen: Withdrawal,
    navigationOptions: {
      drawerIcon: <Icon type="FontAwesome" name="money" style={{fontSize: 20}}/>
    }
  },
},
{
  initialRouteName: 'Home',
  headerMode: 'none',
  navigationOptions: {
      headerVisible: false,
  }
},
{
  contentComponent: headerDrawer,
});

const RootDrawerCon = createAppContainer(RootDrawer);

export default RootDrawerCon;

const styles = StyleSheet.create({
  menuIcon:{
    height: 24,
    width: 24,
  },
  container: {
    alignItems: 'center',
  },
  headerContainer: {
      height: 150,
  },
  headerText: {
      color: '#fff8f8',
  },
});