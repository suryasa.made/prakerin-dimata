import React, {Component} from 'react';
import { Image, ScrollView, StyleSheet } from 'react-native';
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text, Card, CardItem } from 'native-base';

type Props = {};

export default class GuestDetail extends Component<Props> {

  constructor(props){
    super(props);
    this.state = {
      commission: '',
      photo: '',
      photoReq: '',
      dataGuest: [],
    }
  }

  componentDidMount(){
    const { params } = this.props.navigation.state;
    const idUser = params ? params.idUser : null;

    fetch('http://geto.wahanawar.com/operator/user-detail.php?idUser=' + String(idUser))
    .then((responsePro) => responsePro.json())
    .then((responseJsonPro) => {
      this.setState(
        {
          dataGuest: responseJsonPro,
        }
      );
    })
    .catch(()=>(console.warn('error')));
  }
  
  CallGuest(){
    return(
      <ScrollView>
      {
        this.state.dataGuest.map((item, index) => {
        return(
      <Card key={index}>
        <CardItem>
          <Left>
            <Body>
              <Text style={{fontSize: 20, fontWeight: 'bold', color: '#262626'}}>{item.namaUser}'s Bio</Text>
              <Text>Tanggal Lahir: {item.tanggalLahir}</Text>
              <Text>Jenis Kelamin: {item.jenisKelamin}</Text>
              <Text>Email: {item.email}</Text>
              <Text>Alamat: {item.alamat}</Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem cardBody>
          {item.photoUser ? <Image source={{uri : 'http://geto.wahanawar.com/user/'+item.photoUser}} style={{height: 200, width: null, flex: 1}}/> : <Image source={{uri : 'http://geto.wahanawar.com/user/img/nopic.png'}} style={{height: 200, width: null, flex: 1}}/>}
        </CardItem>
      </Card>
        );
      }
      )
    }
    </ScrollView>
    )
  }

  render() {
    const { params } = this.props.navigation.state;
    const idUser = params ? params.idUser : null;
    const username = params ? params.username : null;
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
            onPress = {() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>{username}</Title>
          </Body>
          <Right/>
        </Header>
        <Content style = {styles.container}>
            {this.CallGuest()}
        </Content>
      </Container>
    );
  }
}


const styles = StyleSheet.create({  
  container: {  
      flex: 1,
      padding: 10,
  },
  vim: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
    margin: 16,
    borderRadius: 37.5,
    borderColor: '#CED0CE',
    borderWidth: 1,
  },
  item: { 
    padding: 5,
    fontSize: 14,
  },
  sampul: {
    paddingTop: 0
  }  
})  