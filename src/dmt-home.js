import React, {Component} from 'react';
import { ScrollView, StyleSheet, Text,  Dimensions } from 'react-native';
import {Container, Header, Title, Button, Left, Right, Body, Icon, Content, Card, CardItem} from 'native-base';
import { LineChart } from 'react-native-chart-kit';

type Props = {};

const chartConfig = {
  backgroundGradientFrom: '#1E2923',
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: '#08130D',
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
  strokeWidth: 2 // optional, default 3
}

const screenWidth = 0.88*(Dimensions.get('window').width);

export default class Home extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      pickerSelection: 'Default value!',
      pickerDisplayed: false,
      res: [],
      label: [],
      data: [],
    }
  }

  componentDidMount(){
    fetch('http://geto.wahanawar.com/operator/home.php')
    .then((response) => response.json())
    .then((responseData) => responseData.map((arr) => {
      this.setState(
        {
          // label: arr.month,
          // data: arr.data
        }
      );
    })
    )
    .catch(()=>(console.warn('error')));
  }

  setPickerValue(newValue) {
    this.setState({
      pickerSelection: newValue
    })

    this.togglePicker();
  }

  togglePicker() {
    this.setState({
      pickerDisplayed: !this.state.pickerDisplayed
    })
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button 
              transparent
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Home</Title>
          </Body>
          <Right />
        </Header>
        <Content style = {styles.container}>
          <ScrollView>
          <Card>
            <CardItem header bordered>
              <Text>Statistik Pengunjung</Text>
            </CardItem>
            <CardItem cardBody>
                <LineChart
                  data={{
                    // labels: [this.state.label],
                    labels: ["Agustus","Oktober"],
                    datasets: [{
                      // data: [this.state.data],
                      data: [1, 2],
                    }]
                  }}
                  width={screenWidth}
                  height={220}
                  chartConfig={chartConfig}
                  style={styles.lineChart}
                />
            </CardItem>
          </Card>
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  lineChart:{
    padding: 10,
  }
});