import React, {Component} from 'react';
import {Alert, View, ScrollView, StyleSheet} from 'react-native';
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text, Card, CardItem } from 'native-base';

export default class WithdrawalDetail extends Component<{}> {
  
  constructor (props){
    super (props);
    this.state = {
      saldo: '',
      jumlah: '',
      noRekening: '',
      tanggalPengajuan: '',
      dataSource: [],
    }
    this.arrayHolder = [];
    this.componentDidMount();
  }

componentDidMount(){
  const { params } = this.props.navigation.state;
  const idWithdraw = params ? params.idWithdraw : null;
  const idUser = params ? params.idUser : null;

  return fetch('http://geto.wahanawar.com/operator/withdraw-view-status.php?idUser=' + String(idUser) + '&idWithdraw=' + String(idWithdraw))
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState(
          {
            dataSource: responseJson,
          },
          function(){
            this.arrayHolder = responseJson;
          }
        );
      })
      .catch(()=>(console.warn('error')));
}

actionButton(status){  
  const { params } = this.props.navigation.state;
  const idWithdraw = params ? params.idWithdraw : null;
  fetch('https://geto.wahanawar.com/operator/withdraw-update-status.php?idWithdraw='+ String(idWithdraw) +'&status=' +String(status))
  .then((response) => response.json())
    .then((responseJson) => {
      Alert.alert(responseJson);
    })
    .catch((error) => {
      console.error(error);
    });
}

  render() {
    const { params } = this.props.navigation.state;
    const idWithdraw = params ? params.idWithdraw : null;

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
            onPress = {() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>{idWithdraw}</Title>
          </Body>
          <Right/>
        </Header>
        <Content style = {styles.container}>
          <ScrollView>
            {this.state.dataSource.map((item, index) => {
              return(
                  <Card key={index}>
                    <CardItem>
                      <Body>
                          <Text style = {styles.item}> Saldo : Rp. {item.saldo} </Text>
                          <Text style = {styles.item}> Tagihan : Rp. {item.jumlah} </Text>
                          <Text style = {styles.item}> Status : {item.status} </Text>
                          <View style = {styles.button}>
                            <Button style = {{marginRight: 125}} onPress = {() => this.actionButton('disetujui')}>
                              <Text>Setujui</Text>
                            </Button>
                            <Button onPress = {() => this.actionButton('ditolak')}>
                              <Text>Tolak</Text>
                            </Button>
                          </View>
                      </Body>
                    </CardItem>
                  </Card>
                )
              })
            }
          </ScrollView>
        </Content>
      </Container>
    );
  }
}


const styles = StyleSheet.create({  
  container: {  
      flex: 1,
      padding: 10,
  },
  vim: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
    margin: 16,
    borderRadius: 37.5,
    borderColor: '#CED0CE',
    borderWidth: 1,
  },
  item: { 
    padding: 5,
    fontSize: 14,
  },
  sampul: {
    paddingTop: 0
  } ,
  button: {
    flex: 1,
    flexDirection: 'row',
  }
})  