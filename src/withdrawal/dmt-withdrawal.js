import React, {Component} from 'react';
import {SectionList, View, ScrollView, StyleSheet} from 'react-native';
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text, CardItem } from 'native-base';

export default class Withdrawal extends Component<{}> {

  constructor(props){
    super(props);
    this.state = {
      dataSource: [],
      title: '',
    },
    this.arrayHolder = [];
    this.componentDidMount();
  }

  componentDidMount(){
    return fetch('http://geto.wahanawar.com/operator/withdraw-view1.php')
      .then((response) => response.json())
      .then((responseJson) => {
        const dataSource = responseJson.reduce(function(sections, item){

          let section = sections.find(section => section.tanggalPengajuan === item.tanggalPengajuan);

          if(!section){
            section = { tanggalPengajuan : item.tanggalPengajuan, data : [] };
            sections.push(section);
          }

          section.data.push(item);

          return sections;
        }, []);

        this.setState(
          {
            dataSource: dataSource
          }
        );

      })
      .catch((error) => {
        console.warn(error)
      });
  }

  renderSeparator=()=>{
    return(
      <View
        style={{height:1, width: "100%", backgroundColor: "#CED0CE"}}
      />
    );
  }

  WithdrawalList(){
    return(
      <SectionList
        renderItem={
          ({item, index}) => (
            <Text key={index} style={styles.item} onPress={()=>this.props.navigation.navigate('WithdrawalDetail',{
                  idWithdraw: item.idWithdraw,
                  idUser: item.idUser
                })}>
              ID Withdraw: {item.idWithdraw}
            </Text>)
        }

        renderSectionHeader={({section : {tanggalPengajuan}}) => (
          <View style={styles.bgSection}>
            <Text style={styles.section}>
              {tanggalPengajuan}
            </Text>
          </View>
        )}

        sections = { this.state.dataSource }
        keyExtractor={(item, index) => item + index}
        ItemSeparatorComponent={this.renderSeparator}
      />
    )
  }

  render() {
    const { params } = this.props.navigation.state;
    const key = params ? params.key : null;
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
            onPress = {() => this.props.navigation.openDrawer()}>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Withdrawal</Title>
          </Body>
          <Right />
        </Header>
        <Content style = {styles.container}>
          <View style={styles.bgSectionTitle}>
            <Text style={styles.sectionTitle}>
              Tanggal Pengajuan
            </Text>
          </View>
          <ScrollView>
            {this.WithdrawalList()}
          </ScrollView>
        </Content>
      </Container>
    );
  }
}


const styles = StyleSheet.create({  
  container: {  
      flex: 1,
  },
  vim: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
    margin: 16,
    borderRadius: 37.5,
    borderColor: '#CED0CE',
    borderWidth: 1,
  },
  item: {  
      padding: 10,  
      fontSize: 16,  
      height: 44,  
  }, 
  section: {
    fontSize: 16,
    height: 35,
    textAlignVertical: 'center',
    marginLeft: 10,
  },
  bgSection: {
    backgroundColor: '#e6e6e6',
  },
  sectionTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    height: 35,
    textAlignVertical: 'center',
    marginLeft: 10,
    color: '#ffffff'
  },
  bgSectionTitle: {
    backgroundColor: '#494C53',
  }
})