import React, { Component } from 'react';
import { Image, AsyncStorage, StyleSheet, Alert, Text } from 'react-native';
import { Container, Button, Content, Form, Item, Input } from 'native-base';
import imageLogo from '../assets/img/logo.png';

export default class Login extends Component<{}> {

    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            user: [],
        };
    }

    componentDidMount(){
        this.testing();
    }

    testing = async() => {
        AsyncStorage.getItem('login')
        .then((value) => this.setState({
            getValue: value
        }))

        const login = await AsyncStorage.getItem('login');

        if(login === 'Partner'){
            this.props.navigation.navigate('');
        }
    }

    pressLogin = () => {
        fetch('http://geto.wahanawar.com/operator/login.php', {
            method: 'POST',
            headers: {
                'Accept':'application/json',
                'Content-Type':'application/json',
            },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
            })
        })
        .then((response) => response.json())
            .then((responseData) => {
                this.setState({
                    user: responseData,
                })
                if(responseData === "Login Failed"){
                    Alert.alert("Username atau Password Salah");
                }
                else
                {
                    if (this.state.user.idJenisUser == '3'){
                        AsyncStorage.setItem('login','Partner');
                        this.props.navigation.navigate('Home');
                    }
                }
            })
            .catch(()=>(console.warn('error')));
    }

  render() {
    return ( 
        <Container>
            <Content>
                <Image source = {imageLogo} style = {styles.logo}/>
                <Form>
                    <Item>
                        <Input 
                            placeholder="Username" 
                            onChangeText={(username) => this.setState({username})}
                            value={this.state.username}
                            />
                    </Item>
                    <Item last>
                        <Input 
                        secureTextEntry={true}
                        placeholder="Password" 
                        onChangeText={(password) => this.setState({password})}
                        value={this.state.password}
                        />
                    </Item>
                </Form>
                <Button 
                    success
                    style={styles.button}
                    onPress={() => this.pressLogin()}
                    >
                    <Text style={styles.buttonText}>Login</Text>
                </Button>
            </Content>
        </Container>
    );
  }
}

const styles = StyleSheet.create({
    logo: {
        flex: 1,
        width: '50%',
        resizeMode: 'contain',
        alignSelf: 'center',
        paddingTop: 200,
    },
    button: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: 100,
        marginTop: 50,
        marginLeft: '35%',
    },
    buttonText: {
        fontSize: 18,
        color: '#fff',
    }
})