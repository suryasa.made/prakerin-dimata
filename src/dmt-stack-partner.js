import { createStackNavigator, createAppContainer } from 'react-navigation';

import Destination from './partner/dmt-destination';
import Partner from './partner/dmt-partner';
import PartnerDetail from './partner/dmt-partner-detail';
import SettingsPartner from './partner/dmt-partner-settings';
import Commission from './partner/dmt-partner-comm';

const RootPartnerStack = createStackNavigator({
  Destination: Destination,
  Partner: Partner,
  PartnerDetail: PartnerDetail,
  SettingsPartner: SettingsPartner,
  Commission: Commission,
},
{
  initialRouteName: 'Destination',
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false
  }
});

const RootPartnerStackCon = createAppContainer(RootPartnerStack);

export default RootPartnerStackCon;
