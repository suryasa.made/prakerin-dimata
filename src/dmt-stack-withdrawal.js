import { createStackNavigator, createAppContainer } from 'react-navigation';

import Withdrawal from './withdrawal/dmt-withdrawal';
import WithdrawalDetail from './withdrawal/dmt-withdrawal-detail';

const RootWithdrawalStack = createStackNavigator({
    Withdrawal: Withdrawal,
    WithdrawalDetail: WithdrawalDetail,
},
{
  initialRouteName: 'Withdrawal',
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false
  }
});

const RootWithdrawalStackCon = createAppContainer(RootWithdrawalStack);

export default RootWithdrawalStackCon;
