import { createStackNavigator, createAppContainer } from 'react-navigation';

import Guest from './guest/dmt-guest';
import GuestDetail from './guest/dmt-guest-detail';

const RootGuestStack = createStackNavigator({
  Guest: Guest,
  GuestDetail: GuestDetail,
},
{
  initialRouteName: 'Guest',
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false
  }
});

const RootGuestStackCon = createAppContainer(RootGuestStack);

export default RootGuestStackCon;
