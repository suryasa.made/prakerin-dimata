import React, {Component} from 'react';
import {Image, ScrollView, StyleSheet} from 'react-native';
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text, Card, CardItem } from 'native-base';

export default class PartnerDetail extends Component<{}> {
  constructor(props){
    super(props);
    this.state = {
      commission: '',
      photo: '',
      photoReq: '',
      partner: [],
      dataProduct: [],
    }
  }

componentDidMount(){
  const { params } = this.props.navigation.state;
  const idUser = params ? params.idUser : null;
  const idDestination = params ? params.idDestination : null;
  fetch('http://geto.wahanawar.com/operator/user-detail.php?idUser=' + String(idUser))
    .then((responseP) => responseP.json())
    .then((responseJsonP) => {
      this.setState(
        {
          partner: responseJsonP,
        }
      );
    })

  fetch('http://geto.wahanawar.com/operator/partner-product.php?idUser=' + String(idUser) + '&idDestination=' + String(idDestination))
    .then((responsePro) => responsePro.json())
    .then((responseJsonPro) => {
      this.setState(
        {
          dataProduct: responseJsonPro,
        }
      );
    })
    .catch(()=>(console.warn('Data Kosong')));
}

  render() {
  const { params } = this.props.navigation.state;
  const idUser = params ? params.idUser : null;
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
              onPress = {() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            {this.state.partner.map((item, index) => {
              return(
                <Title key={index}>{item.namaUser}</Title>
            )})}
          </Body>
          <Right>
            <Button transparent
              onPress ={() => this.props.navigation.navigate('SettingsPartner', {
              idUser: idUser
              })}>
              <Icon name='settings'/>
            </Button>
          </Right>
        </Header>
        <Content style = {styles.container}>
          <ScrollView>
            {
              this.state.dataProduct.map((item, index) => {
              return(
            <Card key={index}>
              <CardItem>
                <Left>
                  <Body>
                    <Text>{item.namaProduct}</Text>
                  </Body>
                </Left>
              </CardItem>
              <CardItem cardBody>
                <Image source={{uri : 'http://geto.wahanawar.com/product/'+item.photo}} style={{height: 200, width: null, flex: 1}}/>
              </CardItem>
              <CardItem>
                <Text>Price: Rp. {item.harga}</Text>
              </CardItem>
              <CardItem>
                <Left>
                  <Button primary key={index} onPress={()=>this.props.navigation.navigate('Commission', {
                    idProduct: item.idProduct,
                    i: item.harga
                    })}>
                    <Icon active name="cash" />
                      <Text style={styles.textButton}>Set Commission</Text>
                  </Button>
                </Left>
                <Body/>
              </CardItem>
            </Card>
              );
            }
            )
          }
          </ScrollView>
        </Content>
      </Container>
    );
  }
}


const styles = StyleSheet.create({  
  container: {  
      flex: 1,
      padding: 10,
  },
  vim: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
    margin: 16,
    borderRadius: 37.5,
    borderColor: '#CED0CE',
    borderWidth: 1,
  },
  item: { 
    padding: 5,
    fontSize: 14,
  },
  sampul: {
    paddingTop: 0
  },
  textButton:{
    marginLeft: -25,
  }
})  