import React, {Component} from 'react';
import { FlatList, View, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text } from 'native-base';
import Modal from "react-native-modal";

export default class Destination extends Component<{}> {

  constructor (props){
    super (props);
    this.state = {
      idUser: '',
      idDestination: '',
      namaDestination: '',
      alamatDestination: '',
      photoDestination: '',
      waktuBuka: '',
      waktuTutup: '',
      namaUser: '',
      alamat: '',
      username: '',
      email: '',
      password: '',
      photoUser: '',
      tanggalLahir: '',
      idJenisUser: '',
      jenisKelamin: '',
      tanggalRegister: '',
      modalVisible: false,
      dataPartner: [],
    }
    this.arrayHolder = [];
    this.componentDidMount();
  }

  componentDidMount(){
    return fetch('http://geto.wahanawar.com/operator/destination-data.php')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState(
          {
            dataSource: responseJson,
          },
          function(){
            this.arrayHolder = responseJson;
          }
        );
      })
      .catch(()=>(console.warn('error')));
  }
  
  renderSeparator=()=>{
    return(
      <View
        style={{height:1, width: "100%", backgroundColor: "#CED0CE"}}
      />
    );
  }

  getPartner = (namaDestination) => {
    this.setState({modalVisible: true});
    fetch('http://geto.wahanawar.com/operator/destination-partner.php?namaDestination=' + String(namaDestination).trim())
        .then((response) => response.json())
        .then((responseJson) => {
        this.setState(
            {
                dataPartner: responseJson,
            },
        );
        })
  }
  
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
            onPress = {() => this.props.navigation.openDrawer()}>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Destination</Title>
          </Body>
          <Right />
        </Header>

        <Modal isVisible={this.state.modalVisible}>
          <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
            <Text style={{textAlign: "center", fontWeight: 'bold', marginVertical: 10}}>Data Partner</Text>
            <FlatList
              data={this.state.dataPartner}
              renderItem={({item}) =>
              
              <TouchableOpacity style={{padding: 10, borderBottomColor: '#bdbdbd', borderBottomWidth: 1, marginHorizontal: 10}}>
                  <Text 
                    onPress={()=>
                      {
                        this.props.navigation.navigate('PartnerDetail', {
                          idUser: item.idUser,
                          idDestination: item.idDestination,
                          })
                        this.setState({modalVisible: false})
                      }
                  }
                  style={styles.item}>
                    
                    {item.namaUser}

                  </Text>
                </TouchableOpacity>
                }
              keyExtractor ={(item, index)=> index.toString()}
              ItemSeparatorComponent={this.renderSeparator}
            />

            <TouchableOpacity style={{backgroundColor: '#03A9F4', padding: 10}} onPress={()=>{this.setState({modalVisible: false})}}>
              <Text style={{textAlign: 'center', color: '#FFFFFF'}}>BACK</Text>
            </TouchableOpacity>
          </View>
        </Modal>

        <Content style = {styles.container}>
          <ScrollView>
            <FlatList
              data = {this.state.dataSource}
              renderItem={({item}) => 
              <TouchableOpacity onPress={()=>this.getPartner(item.namaDestination)}>
                <Text
                style={styles.item}>
                  {item.namaDestination}
                </Text>
              </TouchableOpacity>
              }
              keyExtractor ={(item, index)=> index.toString()}
              ItemSeparatorComponent={this.renderSeparator}
            />
          </ScrollView>
        </Content>
      </Container>
    );
  }
}


const styles = StyleSheet.create({  
  container: {  
      flex: 1,
      padding: 10,
  },
  vim: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
    margin: 16,
    borderRadius: 37.5,
    borderColor: '#CED0CE',
    borderWidth: 1,
  },
  item: {  
      padding: 10,  
      fontSize: 16,  
      height: 44,  
  }, 
  sampul: {
    paddingTop: 0
  }  
})  