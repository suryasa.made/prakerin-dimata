import React, {Component} from 'react';
import { ScrollView, StyleSheet, Text, Alert } from 'react-native';
import {Container, Header, Title, Button, Left, Right, Body, Icon, Content, Form, Item, Picker, ListItem} from 'native-base';

export default class SettingsPartner extends Component<{}> {

  constructor(props){
    super(props);
    this.state = {
      status: '',
      res:[],
      s:'',
    };
  }
  
  componentDidMount(){
    const { params } = this.props.navigation.state;
    const idUser = params ? params.idUser : null;
    fetch('http://geto.wahanawar.com/operator/user-view-status.php?idUser=' + String(idUser))
    .then((response) => response.json())
    .then((responseData) => {
      this.setState({
        res: responseData,
        s: this.state.res.status,
      })
    })
  }

  onValueChange2(value: String){
    this.setState({
      status: value
    });
  }

  updateStatus = () => {
    const { params } = this.props.navigation.state;
    const idUser = params ? params.idUser : null;
    fetch('http://geto.wahanawar.com/operator/user-update-status.php?idUser=' + String(idUser) + '&status=' + this.state.status, {
        method: 'POST',
        headers: {
            'Accept':'application/json',
            'Content-Type':'application/json',
        },
        body: JSON.stringify({
            idUser: idUser,
            status: this.state.status,
        })
    })
    .then((response) => response.json())
    .then((responseData) => {
            if(responseData == "failed"){
              Alert.alert("Status Update Failed");
            }else{
              Alert.alert(responseData);
            }
        })
        .catch(()=>(console.warn('error')));
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button 
              transparent
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Settings</Title>
          </Body>
          <Right />
        </Header>
        <ScrollView>
        <Content>
          <ListItem icon>
            <Left>
              <Text>Status</Text>
            </Left>
            <Body>
              <Form>
                <Item picker>
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="arrow-down" />}
                    style={{width: undefined}}
                    placeholder="Status"  
                    placeholderStyle={{color: "bfc6ea"}}
                    placeholderIconColor="007aff"
                    selectedValue={this.state.status}
                    onValueChange={this.onValueChange2.bind(this)}
                  >
                    <Picker.Item label="Active" value="active"/>
                    <Picker.Item label="Blacklist" value="blacklist"/>
                    <Picker.Item label="Drop" value="drop"/>
                  </Picker>
                </Item>
              </Form>
            </Body>
          </ListItem>
          <Button primary
            style={styles.button}
            onPress={() => this.updateStatus()}>
            <Text style={styles.buttonText}>Save</Text>
          </Button>
        </Content>
        </ScrollView>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 15,
    justifyContent: 'center',
  },
  button: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      width: 100,
      marginTop: 50,
      marginLeft: '35%',
  },
  buttonText: {
      fontSize: 18,
      color: '#fff',
  },
  });