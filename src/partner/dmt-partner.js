import React, {Component} from 'react';
import {Alert, FlatList, View, ScrollView, StyleSheet} from 'react-native';
import { Container, Header, Title, Content, Button, Left, Right, Body, Icon, Text } from 'native-base';

export default class Partner extends Component<{}> {

  constructor (props){
    super (props);
    this.state = {
      idUser: '',
      idDestination: '',
      namaDestination: '',
      alamatDestination: '',
      photoDestination: '',
      waktuBuka: '',
      waktuTutup: '',
      namaUser: '',
      alamat: '',
      username: '',
      email: '',
      password: '',
      photoUser: '',
      tanggalLahir: '',
      idJenisUser: '',
      jenisKelamin: '',
      tanggalRegister: '',
    }
    this.arrayHolder = [];
    this.componentDidMount();
  }

  componentDidMount(){
    const { params } = this.props.navigation.state;
    const namaDestination = params ? params.namaDestination : 'null';

    return fetch('http://geto.wahanawar.com/operator/destination/partnerDestination.php?namaDestination=' + String(namaDestination) )
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState(
          {
            dataSource: responseJson,
          },
          function(){
            this.arrayHolder = responseJson;
          }
        );
      })
      .catch(()=>(console.warn('error')));
  }
  
    renderSeparator=()=>{
    return(
      <View
        style={{height:1, width: "100%", backgroundColor: "#CED0CE"}}
      />
    );
  }

  PartnerList(){
    return(
    <FlatList
              data={[
                {key: 'Partner 1'},
                {key: 'Partner 2'},
                {key: 'Partner 3'},
              ]}
              renderItem={({item}) => 
              <Text 
                onPress={()=>this.props.navigation.navigate('PartnerDetail', {
                key: item.key
                })}
              style={styles.item}>
                {item.key}
              </Text>}
              keyExtractor ={(item, index)=> index.toString()}
              ItemSeparatorComponent={this.renderSeparator}
            />
    )
  }

buttonCommission(){
  const { params } = this.props.navigation.state;
  const key = params ? params.key : null;
      Alert.alert('hey '+key);
}

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent
            onPress = {() => this.props.navigation.goBack()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>{namaDestination}</Title>
          </Body>
          <Right />
        </Header>
        <Content style = {styles.container}>
          <ScrollView>
            {this.PartnerList()}
          </ScrollView>
        </Content>
      </Container>
    );
  }
}


const styles = StyleSheet.create({  
  container: {  
      flex: 1,
      padding: 10,
  },
  vim: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
    margin: 16,
    borderRadius: 37.5,
    borderColor: '#CED0CE',
    borderWidth: 1,
  },
  item: {  
      padding: 10,  
      fontSize: 16,  
      height: 44,  
  }, 
  sampul: {
    paddingTop: 0
  }  
})  