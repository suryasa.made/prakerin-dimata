import React, {Component} from 'react';
import { View, StyleSheet, Text, Alert} from 'react-native';
import {Container, Header, Title, Button, Left, Right, Body, Icon, Content, Form, Item, Picker, ListItem} from 'native-base';

export default class Commission extends Component<{}> {

  constructor(props){
    super(props);
    this.state={
      commSelected1: undefined,
      commSelected2: undefined,
      fixed: false,
      persen: false,
    };
    this.fixItems=["0","10000","15000","20000","25000", "30000", "35000", "40000", "45000", "50000"];
    this.persenItems=["0","10","20","30","40", "50"];
    this.componentDidMount();
  }

  componentDidMount(){
    const { params } = this.props.navigation.state;
    const idProduct = params ? params.idProduct : null;
    return(
      <Text>{idProduct}</Text>
    );
  }

  onCommChange1(value: String){
    this.setState({
      commSelected1: value,
    });
  }

  onCommChange2(value: String){
    this.setState({
      commSelected2: value,
    });
  }

  updateComm(){
    const { params } = this.props.navigation.state;
    const idProduct = params ? params.idProduct : null;

    fetch('http://geto.wahanawar.com/operator/product-update-commission.php?idProduct=' + String(idProduct) + '&commission_fixed=' + String(this.state.commSelected1) + '&commission_persen=' + String(this.state.commSelected2))
    .then((response) => response.json())
      .then((responseJson) => {
        Alert.alert(responseJson);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    
    return (
      <Container>
        <Header>
          <Left>
            <Button 
              transparent
              onPress={() => this.props.navigation.goBack()}
            >
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Commission</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <ListItem icon>
            <Left>
              <Text style={{marginTop: 5}}> Fixed (IDR)</Text>
            </Left>
            <Body>
              <Form>
                <Item picker>
                  <Picker
                    mode="dialog"
                    style={{width: 100, height: 50}}
                    selectedValue={this.state.commSelected1}
                    onValueChange={this.onCommChange1.bind(this)}
                  >
                    {this.fixItems.map((item, index) =>
                      (
                        <Picker.Item key={index} label={item} value={item} />
                      )
                    )
                    }
                  </Picker>
                </Item> 
              </Form>
            </Body>
          </ListItem>
          <ListItem icon>
            <Left>
              <Text style={{marginTop: 5}}> Persentase (%)</Text>
            </Left>
            <Body>
              <Form>
                <Item picker>
                  <Picker
                    mode="dialog"
                    style={{width: 100, height: 50}}
                    selectedValue={this.state.commSelected2}
                    onValueChange={this.onCommChange2.bind(this)}
                  >
                    {this.persenItems.map((item, index) =>
                      (
                        <Picker.Item key={index} label={item} value={item} />
                      )
                    )
                    }
                  </Picker>
                </Item> 
              </Form>
            </Body>
          </ListItem>

          <Button primary style = {styles.button} onPress = {() => this.updateComm()}>
            <Text style={styles.buttonText}>Save</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 15,
    justifyContent: 'center',
  },
    button: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: 100,
        marginTop: 50,
        marginLeft: '35%',
    },
    buttonText: {
        fontSize: 18,
        color: '#fff',
    },
  });