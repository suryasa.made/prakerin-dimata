// import React, {Component} from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import Drawer from './src/dmt-root';
import Login from './src/dmt-login';

const RootStack = createStackNavigator({
  Drawer: Drawer,
  Login: Login
},
{
  initialRouteName: 'Login',
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false
  }
});

const RootStackCon = createAppContainer(RootStack);

export default RootStackCon;
